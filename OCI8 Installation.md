# How to install OCI8 on Ubuntu 16.04 and PHP 7.1
Source: http://www.syahzul.com/2016/04/06/how-to-install-oci8-on-ubuntu-14-04-and-php-5-6/

## Install Oracle Instant Client and SDK
### Step 1

Download the Oracle Instant Client and SDK from Oracle website.
(Need to login in Oracle page)

[http://www.oracle.com/technetwork/topics/linuxx86-64soft-092277.html](http://www.oracle.com/technetwork/topics/linuxx86-64soft-092277.html)

Files: `instantclient-basic-linux.x64-12.1.0.2.0.zip` and `instantclient-sdk-linux.x64-12.1.0.2.0.zip`.

### Step 2

Create a new folder to store Oracle Instant Client zip files on your server.

Upload the Instant Clients files inside this folder.

```
mkdir /opt/oracle
```

### Step 3

Now we need to extract the files.

```
cd /opt/oracle
unzip instantclient-basic-linux.x64-12.1.0.2.0.zip
unzip instantclient-sdk-linux.x64-12.1.0.2.0.zip
```

### Step 4

Next, we need to create a symlink to Instant Client files.

```
ln -s /opt/oracle/instantclient_12_1/libclntsh.so.12.1 /opt/oracle/instantclient_12_1/libclntsh.so
ln -s /opt/oracle/instantclient_12_1/libocci.so.12.1 /opt/oracle/instantclient_12_1/libocci.so
```

### Step 5

Add the folder to our `ldconfig`.

```
echo /opt/oracle/instantclient_12_1 > /etc/ld.so.conf.d/oracle-instantclient
```

### Step 6

Update the Dynamic Linker Run-Time Bindings

```
ldconfig
```

Done. Now we can proceed to the next part.


## Install Additional Packages

To install the OCI8 extension, we need to install some additional package on our server.

### Step 1

Run these command:

```
apt-get install php-dev php-pear build-essential libaio1
```

### Step 2

Once installed, we need to get the OCI8 file.


```
pecl install oci8
```


When you are prompted for the Instant Client location, enter the following:

```
instantclient,/opt/oracle/instantclient_12_1
```

### Step 3

We need to tell PHP to load the OCI8 extension.

```
echo "extension = oci8.so" >> /etc/php/7.1/fpm/php.ini
echo "extension = oci8.so" >> /etc/php/7.1/cli/php.ini
```

### Step 4

Check if the extension is enabled.

```
php -m | grep 'oci8'
```

If returns `oci8`, its works!

### Step 5

Restart the PHP-FPM

```
service php7.1-fpm restart
```

Now you can connect to Oracle DBMS from your PHP applications.

## Pecl plugin installation for multiple php versions


I have installed both php5.6 and php7.0 from PPA on Ubuntu according to this manual

http://lornajane.net/posts/2016/php-7-0-and-5-6-on-ubuntu

But I didn't get how to install extensions using pecl for php5.6 or php7.0.

For example I have already installed version of libevent or amqp in php5.6.

Now when I type pecl install libevent and my active php version is php7.0 (using update-alternatives --set php /usr/bin/php7.0),peclreturns message thatlibevent` already installed.

But it was installed only for php5.6 (when this version was active) and now I want to do it for php7.0.

Which commands could help me?

UPD

I have found this commands for switch pecl to php7.0 and packet them to executable bash scripts:

### For php7.0

```
#!/bin/bash

sudo update-alternatives --set php /usr/bin/php7.0

sudo pecl config-set php_ini /etc/php/7.0/cli/php.ini
sudo pecl config-set ext_dir /usr/lib/php/20151012/
sudo pecl config-set bin_dir /usr/bin/
sudo pecl config-set php_bin /usr/bin/php7.0
sudo pecl config-set php_suffix 7.0
and for php5.6
```

### For php5.6
```
#!/bin/bash

sudo update-alternatives --set php /usr/bin/php5.6

sudo pecl config-set php_ini /etc/php/5.6/cli/php.ini
sudo pecl config-set ext_dir /usr/lib/php/20131226/
sudo pecl config-set bin_dir /usr/bin/
sudo pecl config-set php_bin /usr/bin/php5.6
sudo pecl config-set php_suffix 5.6
```

### After setting the above config settings use the following commands
```
$ pecl -d php_suffix=5.6 install <package>
$ pecl uninstall -r <package>

$ pecl -d php_suffix=7.0 install <package>
$ pecl uninstall -r <package>

$ pecl -d php_suffix=7.1 install <package>
$ pecl uninstall -r <package>
```